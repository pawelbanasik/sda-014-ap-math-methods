package com.pawelbanasik;

public class MyMathMain {

    public static void main(String[] args) {
        System.out.println(MyMath.max(3, 10));
        System.out.println(MyMath.max(3L, 10L));
        System.out.println(MyMath.max(3f, 10f));
        System.out.println(MyMath.max(3d, 10d));
        System.out.println(MyMath.max(0.5, 0.25));
        System.out.println(MyMath.pow(3,4));
    }
}
